#!/bin/bash

# Move into the scripts directory
SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd $SCRIPTS_DIR

if [ -d ../build ]; then
    rm -r ../build
fi