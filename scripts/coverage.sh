#!/bin/bash

# Exit on first failure
set -e

if [ $# -eq 0 ]; then
    echo "Error: The path to the build directory is required"
    exit 1
fi

BUILD_DIR="$(realpath "$1")"
SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Build project in debug mode
"${SCRIPTS_DIR}"/build.sh -b Debug

COV_FOLDER="${SCRIPTS_DIR}"/coverage
if ! [ -d "${COV_FOLDER}" ]; then
    mkdir "${COV_FOLDER}"
fi
GEN_COV_FILES_FOLDER="${BUILD_DIR}"/test

# Generate lcov base information
lcov -c -i -d "${GEN_COV_FILES_FOLDER}" --gcov-tool "${SCRIPTS_DIR}"/llvm-gcov.sh -o "${COV_FOLDER}"/base.info

# Run tests
"${SCRIPTS_DIR}"/unit-tests.sh "${BUILD_DIR}"/bin

# Prepare lcov final information
cd "${SCRIPTS_DIR}"

# Collect the code coverage results
lcov -c -d "${GEN_COV_FILES_FOLDER}" --gcov-tool "${SCRIPTS_DIR}"/llvm-gcov.sh -o "${COV_FOLDER}"/test.info
lcov -a "${COV_FOLDER}"/base.info -a "${COV_FOLDER}"/test.info -o "${COV_FOLDER}"/total.info
lcov --remove "${COV_FOLDER}"/total.info "*build*" "*Debug*" "*conan*" "*External*" "/usr/*" -o "${COV_FOLDER}"/final.info


DATE=`date +%F`
HOUR=`date +%T`

# Generate final html report
REPORT="cov-${DATE}-${HOUR//:}"
genhtml --config-file lcovrc "${COV_FOLDER}"/final.info -o "${COV_FOLDER}"/"${REPORT}"

echo "The coverage report is available at: ${COV_FOLDER}/${REPORT}"
