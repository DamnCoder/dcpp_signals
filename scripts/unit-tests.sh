#!/bin/bash

# Move into tests dir
############################

if [ $# -eq 0 ]; then
    echo "Error: The path to tests directory is required"
    exit 1
fi

cd $1 || exit 1

# Vars
############################
export LD_LIBRARY_PATH=./shared-libs

# Output color stuff
############################

# Check if stdout is a terminal
if test -t 1; then
    # Check color support
    ncolors=$(tput colors)

    if test -n "$ncolors" && test "$ncolors" -ge 8; then
        bold="$(tput bold)"
        normal="$(tput sgr0)"
        red="$(tput setaf 1)"
        green="$(tput setaf 2)"
    fi
fi

# Run all tests
############################

counter=0
passed=0
declare -A results

for testbin in test_*; do
    echo -e "\n\n--------------------------------------"
    echo    "Running $testbin"
    echo -e "--------------------------------------\n"

    if eval "./$testbin"; then
        results[$testbin]="${green}passed${normal}"
        passed=$(($passed + 1))
    else
        results[$testbin]="${red}failed${normal}"
    fi
    counter=$(($counter + 1))
done

failed=$(($counter - $passed))

# Print results
############################

echo
echo "--------------------------------------"
echo "${bold}Global tests summary${normal}"
echo
printf "%s\n" "${!results[@]}" "${results[@]}" | pr -2t
echo
echo "    ${bold}${counter} test executables"
echo "    ${green}${passed} passed"
# Exit with non-zero if some test failed
if [ "$failed" -gt 0 ]; then
    echo "    ${red}${failed} failed"
    echo "${normal}--------------------------------------"
    exit 1
fi
echo "${normal}--------------------------------------"

exit 0
