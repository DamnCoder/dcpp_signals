#!/bin/bash

# Stop on errors
set -e

print_help() {
    echo "usage: $SCRIPTNAME [Options] -b|--build <build_type> -c|--compiler <compiler_type> -p|--profile <conan_profile>"
    echo
    echo "Options:"
    echo "  -b|--build <build_type>  Choose build type: Both (default), Release or Debug"
    echo "  -c|--compiler <compiler_type>  Choose compiler type: gcc (default), gcc or clang"
    echo "  -p|--profile <conan_profile>  Choose your conan profile"
    exit 1
}

SCRIPTNAME=$0
BUILD_TYPE="Both"
COMPILER_TYPE="gcc"
CONAN_PROFILE="gcc_linux"

UNKNOWNARGS=()
while [[ $# -gt 0 ]] 
do

KEY="$1"

case $KEY in
    -b | --build )    
    BUILD_TYPE="$2"; 
    shift  # past arg
    shift  # past value
    ;;

    -c | --compiler )    
    COMPILER_TYPE="$2"; 
    shift  # past arg
    shift  # past value
    ;;

    -p | --profile )    
    CONAN_PROFILE="$2"; 
    shift  # past arg
    shift  # past value
    ;;
    
    *)    # unknown option
    UNKNOWNARGS+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

echo "${BUILD_TYPE}" "${COMPILER_TYPE}" "${CONAN_PROFILE}"

UNKNOWNARGSSIZE=${#UNKNOWNARGS[@]}
if [ ${UNKNOWNARGSSIZE} -gt 0 ]; then
    print_help
fi

if [[ "${COMPILER_TYPE}" == "gcc" ]]; then
    export CC=/usr/bin/gcc
    export CXX=/usr/bin/g++
elif [[ "${COMPILER_TYPE}" == "clang" ]]; then
    #statements
    export CC=/usr/bin/clang
    export CXX=/usr/bin/clang++
fi

# Move into the scripts directory
SCRIPTS_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd $SCRIPTS_DIR

case $BUILD_TYPE in
    Debug|Both)
    # Generate debug project
    cd ..
    mkdir -p build/Debug
    cd build/Debug
    cmake ../.. -DCMAKE_BUILD_TYPE=Debug -DCONAN_PROFILE:STRING="${CONAN_PROFILE}"
    cd ../../scripts
    ;;&

    Release|Both)
    # Generate release project
    cd ..
    mkdir -p build/Release
    cd build/Release
    cmake ../.. -DCMAKE_BUILD_TYPE=Release -DCONAN_PROFILE:STRING="${CONAN_PROFILE}"
    cd ../../scripts
    ;;

    *)
    if [[ ! $BUILD_TYPE =~ Debug ]]
    then
        echo "Unknown build type";
    fi
    ;;
esac
