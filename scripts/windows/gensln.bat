@echo off

cd ..
cd ..

if not exist build\ (
    echo "Creating dir .\build"
    md build
)

if exist build\ (
    cd build\
    set CMAKE_OPTIONS=""
    cmake .. -G "Visual Studio 16 2019" -A x64 -DCMAKE_BUILD_TYPE=Debug -DCONAN_PROFILE:STRING="default" %CMAKE_OPTIONS%
    cd ..
)
cd scripts

