# Project initial configuration
cmake_minimum_required(VERSION 3.5)

project(dcpp_signals LANGUAGES CXX)

# Initial check to abort any attempt of in source build
if( "${PROJECT_SOURCE_DIR}" STREQUAL "${PROJECT_BINARY_DIR}" )
    message(FATAL "no in source building allowed." )
endif()

# Project setup
set(CMAKE_CONFIGURATION_TYPES
	Debug
	Release
	RelWithDebInfo
	MinSizeRel
)

set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")
set(CMAKE_POSITION_INDEPENDENT_CODE ON)
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

# Configure CCache if available
find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
        set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)

if (WIN32) # Windows
    add_compile_options(-DWIN32)

    # Set Release output directory for runtime targerts
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELEASE ${CMAKE_BINARY_DIR}/Release)
    # Use Release directory for RelWithDebInfo and MinSizeRel builds
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_RELWITHDEBINFO ${CMAKE_BINARY_DIR}/Release)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_MINSIZEREL ${CMAKE_BINARY_DIR}/Release)
    # Set Debug output directory for runtime targets
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_DEBUG ${CMAKE_BINARY_DIR}/Debug)

    # Data destination directories
    set(DATA_DEST_WIN_DEB ${CMAKE_BINARY_DIR}/Debug/data)
    set(DATA_DEST_WIN_REL ${CMAKE_BINARY_DIR}/Release/data)
else() # Linux
    if (UNIX AND NOT APPLE)
        add_compile_options(-DLINUX)
    endif()

    # Set output directories for libraries and binaries.
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

    # Data destination directory
    set(DATA_DEST_LINUX ${CMAKE_BINARY_DIR}/bin/data)

    execute_process(COMMAND ${CMAKE_CXX_COMPILER} 
                  -fuse-ld=gold -Wl,--version 
                  ERROR_QUIET OUTPUT_VARIABLE ld_version)
    if ("${ld_version}" MATCHES "GNU gold")
        message(STATUS "Found Gold linker, use faster linker")
        set(CMAKE_EXE_LINKER_FLAGS 
            "${CMAKE_EXE_LINKER_FLAGS} -fuse-ld=gold")
        set(CMAKE_SHARED_LINKER_FLAGS
            "${CMAKE_SHARED_LINKER_FLAGS} -fuse-ld=gold ")
    endif()
    if("${CMAKE_BUILD_TYPE}" STREQUAL "Debug")
        SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -fprofile-arcs -ftest-coverage")
        SET(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS} -O0 -fprofile-arcs -ftest-coverage")
    endif()
endif()

# Conan setup
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/common)
include(conan)
conan_check(VERSION 1.20.0 REQUIRED)

conan_add_remote(
    NAME conan_center_remote
    INDEX 2
    URL https://api.bintray.com/conan/conan/conan-center
)

# Conan dependences installation
if (WIN32)
    conan_cmake_run(
        CONANFILE conanfile.py
        BASIC_SETUP CMAKE_TARGETS NO_OUTPUT_DIRS
        PROFILE ${CONAN_PROFILE}
        BUILD missing)

    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)

else()
    conan_cmake_run(
        CONANFILE conanfile.py
        BASIC_SETUP CMAKE_TARGETS NO_OUTPUT_DIRS
        PROFILE ${CONAN_PROFILE}
        BUILD missing)

    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
endif()

add_subdirectory(sources)
add_subdirectory(test)
