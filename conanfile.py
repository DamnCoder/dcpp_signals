from conans import ConanFile

class DCPPTaskMgr(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = (
        "gtest/1.8.1@",
        "spdlog/1.9.2@"
    )
    generators = "cmake"

    def imports(self):
        if self.settings.os == "Windows":
            if self.settings.build_type == "Debug":
                self.copy("*.dll", src="bin", dst="./Debug")
            else:  # Release
                self.copy("*.dll", src="bin", dst="./Release")
        else:  # Linux
            self.copy("*.so*", src="lib", dst="./bin/shared-libs")
